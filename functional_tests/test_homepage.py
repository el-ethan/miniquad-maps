from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException

from functional_tests.base import FunctionalTest


class TestHomePage(FunctionalTest):

    def test_login_and_signup_links_displayed(self):
        # User visits homepage
        self.browser.get(self.live_server_url)
        # User sees Log in and Sign up links on the page
        # since they are not logged in yet
        self.assertTrue(self.browser.find_element_by_link_text('Log in').is_displayed())
        self.assertTrue(self.browser.find_element_by_link_text('Sign up').is_displayed())

    def test_miniquad_img_moves(self):
        self.browser.get(self.live_server_url)
        mq_image = self.browser.find_element_by_id('miniquad')
        start_location = mq_image.location
        ActionChains(self.browser).move_to_element(mq_image).click(mq_image).perform()
        self.assertNotEqual(mq_image.location, start_location)

    def test_clicking_search_buttons_without_query_shows_popover(self):
        self.browser.get(self.live_server_url)
        self.browser.find_element_by_id('search-btn').click()
        popover = self.wait_for_element_by_class('popover')
        self.assertTrue(popover.is_displayed())

        self.browser.find_element_by_id('spotsearch').click()

        self.wait_for_invisible_by_class('popover')

        self.browser.find_element_by_id('nearby-btn').click()
        popover = self.wait_for_element_by_class('popover')
        self.assertTrue(popover.is_displayed())

    def test_auto_complete_candidates_exist(self):
        self.browser.get(self.live_server_url)
        self.browser.find_element_by_id('spotsearch').send_keys('sunnyside park')
        self.assertTrue(self.wait_for_element_by_class('pac-container').is_displayed())

    def test_adding_spots_from_map(self):
        self.browser.get(self.live_server_url)
        spotmap = self.wait_for_element_by_id('map')
        self.assertTrue(spotmap.is_displayed())
        spotmap.click()
        added_marker = self.wait_for_element_by_xpath("//div[contains(text(), '+')]")
        self.assertTrue(added_marker.is_displayed())
        ActionChains(self.browser).move_to_element(added_marker).click().perform()
        btn = self.wait_for_element_by_xpath("//button[contains(text(), 'Add new spot')]")
        self.assertTrue(btn.is_displayed())
