from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from django.test import LiveServerTestCase


class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Chrome('/usr/bin/chromedriver')
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def wait_for_element_by_id(self, id):
        element = WebDriverWait(self.browser, 10).until(
            EC.visibility_of_element_located((By.ID, id))
        )
        return element

    def wait_for_element_by_class(self, name):
        element = WebDriverWait(self.browser, 10).until(
            EC.visibility_of_element_located((By.CLASS_NAME, name))
        )
        return element

    def wait_for_element_by_xpath(self, path):
        element = WebDriverWait(self.browser, 10).until(
            EC.visibility_of_element_located((By.XPATH, path))
        )
        return element

    def wait_for_invisible_by_class(self, name):
        element = WebDriverWait(self.browser, 10).until(
            EC.invisibility_of_element_located((By.CLASS_NAME, name))
        )
        return element
