from functional_tests.base import FunctionalTest


class TestSignup(FunctionalTest):

    def test_signup_shows_welcome_message_and_displays_correct_links(self):
        # User enters sign up information
        self.browser.get(self.live_server_url + '/signup/')
        username = self.browser.find_element_by_name('username')
        email = self.browser.find_element_by_name('email')
        password = self.browser.find_element_by_name('password')

        test_username = 'test_user'
        username.send_keys(test_username)
        email.send_keys('test@test.edu')
        password.send_keys('my password')

        # User clicks signup
        self.browser.find_element_by_id('submit-btn').click()

        # User sees welcome message
        welcome_modal = self.wait_for_element_by_id('welcomeModal')
        self.assertTrue(welcome_modal.is_displayed())

        self.browser.get(self.live_server_url)
        self.assertIsNotNone(self.browser.find_element_by_link_text('My spots'))
        self.assertIsNotNone(self.browser.find_element_by_link_text('Log out'))
        self.assertIsNotNone(self.browser.find_element_by_link_text(
            'Signed in as: %s' % test_username
        ))
