"""flywithme URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import login as django_login
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

from topspots import urls as spot_urls
from topspots import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^help/', views.help_page, name='help'),
    url(r'^$', views.home_page, name='home'),
    url(r'^user/(\d+)/', views.user_profile, name='user_profile'),
    url(r'^updatelocation/', views.update_location, name='update_location'),
    url(r'^useaddress/', views.use_address, name='use_address'),
    url(r'^spots/', include(spot_urls)),
    url(r'^signup/', views.signup, name='signup'),
    url(r'^login/', django_login, name='login', kwargs={
        'template_name': 'topspots/login.html',
        'redirect_field_name': 'next',
    }),
    url(r'^logout/', views.logout, name='logout'),
    url(r'^favicon.ico$',
        RedirectView.as_view(url=staticfiles_storage.url('topspots/images/favicon.ico'), permanent=False),
        name="favicon"),
    # This is a hack so that users won't have access to django-allauth views I want to block
    url(r'^accounts/email/', views.go_home),
    url(r'^accounts/logout/', views.go_home),
    url(r'^accounts/login/', views.go_home),
    url(r'^accounts/signup/', views.go_home),
    url(r'^accounts/social/connections/', views.go_home),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^tips/', views.tips, name='tips'),
]
