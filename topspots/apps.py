from django.apps import AppConfig


class TopspotsConfig(AppConfig):
    name = 'topspots'
