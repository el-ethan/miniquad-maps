from django.contrib import admin

from .models import Spot, FeaturedVideo

admin.site.register(Spot)
admin.site.register(FeaturedVideo)
