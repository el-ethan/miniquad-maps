var searchBox;
var places;

function initSearchBox() {
    var KCLatLng = new google.maps.LatLng({lat: 39.093201, lng: -94.573419});
    var bounds = new google.maps.LatLngBounds(KCLatLng);

    var input = $('#spotsearch')[0];
    searchBox = new google.maps.places.SearchBox(input);
    searchBox.setBounds(bounds);

    var emptySearchShowPopover = function(e) {
        if($('#spotsearch').val() == "") {
            e.preventDefault();
            $(this).popover({
                html: true,
                content: '<span class="plain">Please enter an address in the search box</span>',
                trigger: "focus",
                placement: "bottom"
            });
            $(this).popover("show");
        }
    };


    $('#nearby-btn').click(emptySearchShowPopover);
    $('#search-btn').click(emptySearchShowPopover);

}

function initSearchBoxAndMap() {
    initSearchBox();
    initMap();
}
