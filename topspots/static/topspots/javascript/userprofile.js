function initLocationUpdateBox() {
    var input = $('#user-address')[0];
    searchBox = new google.maps.places.SearchBox(input);
}

$('#location-edit-button').on('click', function () {
    $input = $('#user-address');
    $input.prop('disabled', false);
    $input.focus();
    $input.select();
});
