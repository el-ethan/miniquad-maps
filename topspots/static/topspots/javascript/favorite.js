
$(document).ready(function() {

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

});

var favorite = function(id) {
    var $spot = $('#spot_score' + id);
    var currentScore = parseInt($spot.text());
    var $favoriteGlyph = $('#glyph' + id);

    if($favoriteGlyph.hasClass('text-muted')) {
        // maybe show user a message
        return;
    }

    var newScore;
    if($favoriteGlyph.hasClass('text-danger')) {
        newScore = currentScore - 1;
        $favoriteGlyph.removeClass('text-danger');
    } else {
        $favoriteGlyph.addClass('text-danger');
        newScore = currentScore + 1;
    }

    $.post('/spots/favorite/', {
        spot_id: id
    });

    $spot.text(newScore);

};
