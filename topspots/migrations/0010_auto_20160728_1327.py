# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-07-28 18:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('topspots', '0009_siteuser_member_since'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spot',
            name='state',
            field=models.CharField(max_length=50),
        ),
    ]
