import os

from django.contrib.auth.models import User
from django.contrib import messages
from django.db import models
from django.utils import timezone
from django.dispatch import receiver
from allauth.account.signals import user_signed_up


GMAPS_JS_API_KEY = os.environ['GMAPS_JS_API_KEY']


class NoAPIKeyError(Exception):
    pass


def get_map_url(address, maptype='roadmap', latlng=None, api_key=None):
    if not api_key:
        raise NoAPIKeyError('Please provide a Google Maps Static Maps API key')
    base_url = 'https://maps.googleapis.com/maps/api/staticmap'
    params = '?center={0}&zoom=15&size=640x300&maptype={1}&markers={0}&key={2}'
    return base_url + params.format(latlng or address, maptype, api_key)


class Spot(models.Model):

    name = models.TextField()
    score = models.IntegerField(default=0)
    url_name = models.TextField(default='')
    created = models.DateTimeField(default=timezone.now)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, default=90)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, default=90)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    @property
    def currently_featured_video(self):
        return self.featuredvideo_set.order_by('-featured_date').first()

    @property
    def map_url(self):
        return get_map_url(
            self.url_name, latlng=self.latlng, api_key=GMAPS_JS_API_KEY
        )

    @property
    def sat_map_url(self):
        return get_map_url(
            self.url_name, latlng=self.latlng,
            maptype='satellite', api_key=GMAPS_JS_API_KEY
        )

    @property
    def small_map_url(self):
        return self.map_url.replace('size=640x300', 'size=300x100')

    @property
    def small_long_map_url(self):
        return self.map_url.replace('size=640x300', 'size=640x100&scale=1')

    @property
    def short_name(self):
        return self.name.split(',')[0]

    @property
    def google_maps_places_url(self):
        _url = 'https://www.google.com/maps/place/'
        return _url + self.url_name

    @property
    def favorited_by_list(self):
        return self.siteuser_set.all()

    @property
    def latlng(self):
        return str(self.latitude) + ',' + str(self.longitude)

    def __str__(self):
        return self.name

    def favorited_by(self, user):
        return user in self.favorited_by_list

    @property
    def ordered_comments(self):
        return self.comment_set.order_by('-created')

    @property
    def total_packs_flown(self):
        total = self.packlog_set.aggregate(models.Sum('packs'))['packs__sum']
        return total or 0

    def packs_flown_by(self, user):
        return self.packlog_set.filter(user=user).aggregate(models.Sum('packs'))['packs__sum'] or 0


class FeaturedVideo(models.Model):
    spot = models.ForeignKey(Spot, on_delete=models.CASCADE)
    html = models.TextField(default='')
    featured_date = models.DateTimeField(auto_now_add=True)


class Comment(models.Model):
    text = models.TextField(default='')
    poster = models.ForeignKey(User, on_delete=models.CASCADE)
    spot = models.ForeignKey(Spot, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)


class CommentReply(models.Model):
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
    poster = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField(default='')
    created = models.DateTimeField(auto_now_add=True)


class SiteUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    member_since = models.DateTimeField(auto_now_add=True)
    favorites = models.ManyToManyField(Spot)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, default=None, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, default=None, null=True)
    address = models.TextField(null=True)
    use_address = models.BooleanField(default=False)

    @property
    def total_packs_logged(self):
        total = self.user.packlog_set.aggregate(models.Sum('packs'))['packs__sum']
        return total or 0


class PackLog(models.Model):
    packs = models.IntegerField()
    datetime_logged = models.DateTimeField(default=timezone.now)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    spot = models.ForeignKey(Spot, on_delete=models.CASCADE)


def create_siteuser(sender, instance, created, **kwargs):
    if created:
        SiteUser.objects.create(user=instance)


@receiver(user_signed_up)
def show_welcome_message(request, user, **kwargs):
    messages.success(request, 'User signup successful.')

models.signals.post_save.connect(create_siteuser, sender=User)
