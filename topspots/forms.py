from django.forms import ModelForm, TextInput

from django.contrib.auth.models import User


class SignUpForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        widgets = {
            'username': TextInput(attrs={
                'name': 'username',
                'placeholder': 'Username',
                'class': 'form-control',
                'type': 'text',
                'autofocus': 'autofocus',
            }),
            'email': TextInput(attrs={
                'name': 'email',
                'placeholder': 'E-Mail Address',
                'class': 'form-control',
                'type': 'text',
            }),
            'password': TextInput(attrs={
                'name': 'password',
                'placeholder': 'Password',
                'class': 'form-control',
                'type': 'password',
            })
        }
