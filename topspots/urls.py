from django.conf.urls import url

from topspots import views

urlpatterns = [
    url(r'details/(\d+)/$', views.spot_detail, name='spot_detail'),
    url(r'search/$', views.spot_search, name='spot_search'),
    url(r'save/$', views.save_spot, name='save_spot'),
    url(r'favorite/$', views.favorite, name='favorite'),
    url(r'(\d+)/comment/$', views.comment, name='comment'),
    url(r'details/(\d+)/comment/(\d+)/delete/$', views.delete_comment, name='delete_comment'),
    url(r'details/(\d+)/comment/(\d+)/reply/$', views.reply_comment, name='reply_comment'),
    url(r'details/(\d+)/comment/(\d+)/reply/delete/$', views.delete_reply, name='delete_reply'),
    url(r'details/(\d+)/comment/(\d+)/edit/$', views.edit_comment, name='edit_comment'),
    url(r'details/(\d+)/submitlink/$', views.submit_video_link, name='submit_video_link'),
    url(r'myspots/(.+)/$', views.my_spots, name='my_spots'),
    url(r'(\d+)/logpacks/$', views.log_packs, name='log_packs'),
]
