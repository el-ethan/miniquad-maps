from django import template

register = template.Library()


@register.filter
def userpacks(spot, user):
    return spot.packs_flown_by(user)
