from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def email_to_username(email):
    return email.split('@')[0]
