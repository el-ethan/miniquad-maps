from django.test import TestCase
from django.contrib.auth.models import User


class AdvancedTestCase(TestCase):

    def get_logged_in_user(self, username='test_user', password='test'):
        user = User.objects.create_user(username=username, password=password)
        self.client.login(username=username, password=password)
        return user
