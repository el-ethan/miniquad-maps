import os

from django.test import TestCase

import config


class TestConfig(TestCase):

    def test_config_values_set(self):
        env_vars = ['GMAPS_JS_API_KEY', 'GMAPS_WEB_GEO_API_KEY', 'FORECAST_API_KEY']
        for var in env_vars:
            self.assertNotEqual(os.environ.get(var), None)
            self.assertNotEqual(os.environ.get(var), '')
