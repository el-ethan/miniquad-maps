from django.test import TestCase

from topspots.services import get_lat_lng, get_nearby_airports,\
    get_location_from_address, get_city_state


class TestGetLocationData(TestCase):

    def test_lat_lng_returns_correct_lat_and_lng(self):
        address = 'Sunnyside Park, Summit Street, KCMO, MO'
        expected_lat = 38.9776715
        expected_lng = -94.5984995

        lat, lng = get_lat_lng(address)

        self.assertEqual(lat, expected_lat)
        self.assertEqual(lng, expected_lng)

    def test_get_city_state(self):

        city, state = get_city_state(38.976133, -94.586202)
        self.assertEqual(city, 'Kansas City')
        self.assertEqual(state, 'MO')

    def test_zero_results_returns_none_none(self):
        address = "Foo, Bar, Baz, Fake Country"
        self.assertEqual(get_lat_lng(address), (None, None))


class TestGetNearbyAirports(TestCase):

    def test_no_nearby_airports(self):
        lat = 31.456609
        lng = -109.930294
        airport_data = get_nearby_airports(lat=lat, lng=lng)
        self.assertEqual(len(airport_data), 0)

    def test_has_nearby_airports(self):
        lat = 39.121515
        lng = -94.823400
        airport_data = get_nearby_airports(lat, lng)
        expected_airports = [
            'Providence Airport',
        ]

        self.assertEqual(len(airport_data), 1)
        for ap in airport_data:
            self.assertIn(ap, expected_airports)


class TestGetLocationFromZip(TestCase):

    def test_get_location_from_address(self):
        expected_loc = (38.9546734, -94.5985613)
        location = get_location_from_address('64114')
        self.assertEqual(expected_loc, location)
