from django.test import TestCase
from django.test.client import RequestFactory

from topspots.helpers import geolocate


class TestGeolocate(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_geolocate_fail(self):
        request = self.factory.get('/', REMOTE_ADDR="127.0.0.1")
        lat, lng, geolocate_fail = geolocate(request)
        self.assertEqual((lat, lng), (39.096915, -94.572250))
        self.assertTrue(geolocate_fail)

    def test_geolocate_without_fail(self):
        request = self.factory.get('/', REMOTE_ADDR="162.243.92.35")
        lat, lng, geolocate_fail = geolocate(request)
        self.assertEqual((lat, lng), (40.7143, -74.006))
        self.assertFalse(geolocate_fail)
