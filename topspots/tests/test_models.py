from django.contrib.auth.models import User

from topspots.tests.base import AdvancedTestCase
from topspots.models import Spot


class TestSpotModel(AdvancedTestCase):

    def test_deleting_user_doesnt_delete_spot(self):
        user = User.objects.create_user(username='test_user', password='password')
        spot = Spot.objects.create(name='Fake Park', city='Fakeville', state='Fake York', added_by=user)

        self.assertEqual(Spot.objects.first().added_by, user)
        User.objects.first().delete()
        self.assertEqual(Spot.objects.get(id=spot.id).added_by, None)
