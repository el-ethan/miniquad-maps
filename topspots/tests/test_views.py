from unittest import skip

from django.contrib.auth.models import User

from topspots.models import Spot, PackLog, Comment, CommentReply
from topspots.tests.base import AdvancedTestCase


class TestHomePage(AdvancedTestCase):

    def test_sort_by_recent(self):
        num_of_spots = 10
        for i in range(num_of_spots):
            Spot.objects.create(name='old' + str(i), score=1)

        for i in range(num_of_spots):
            Spot.objects.create(name='new' + str(i), score=0)

        response = self.client.get('/')
        spots = list(response.context['spots'])
        for spot in spots:
            self.assertIn('old', spot.name)
            self.assertNotIn('new', spot.name)

        response = self.client.get('/?sort=recent')
        spots = list(response.context['spots'])
        for spot in spots:
            self.assertIn('new', spot.name)
            self.assertNotIn('old', spot.name)

    def test_spot_count(self):
        num_of_spots = 20
        for i in range(num_of_spots):
            Spot.objects.create()
        response = self.client.get('/')
        self.assertEqual(response.context['spot_count'], num_of_spots)

    def test_homepage_uses_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'topspots/home.html')

    def test_home_template_inherits_from_base(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'topspots/base.html')

    def test_sorting_without_nearby_spots_is_favorites(self):
        response = self.client.get('/')
        self.assertEqual(response.context['sorting'], 'favorites')

    @skip('Fix after moving to pagination')
    def test_ten_spots_on_homepage(self):
        for i in range(10):
            Spot.objects.create()
        response = self.client.get('/')
        self.assertEqual(len(response.context['spots']), 10)

    @skip('Fix after moving to pagination')
    def test_only_ten_spots_on_homepage(self):
        for i in range(20):
            Spot.objects.create()
        response = self.client.get('/')
        self.assertEqual(len(response.context['spots']), 10)

    @skip('Fix after moving to pagination')
    def test_only_top_ten_spots_on_homepage(self):
        for i in range(10):
            Spot.objects.create(score=2)
        best = Spot.objects.create(score=100)
        too_low = Spot.objects.create(score=1)
        response = self.client.get('/')
        self.assertNotIn(too_low, response.context['spots'])
        self.assertIn(best, response.context['spots'])

    @skip('Fix after moving to pagination')
    def test_top_rated_spots_first(self):
        for i in range(5):
            Spot.objects.create()
        top_rated_spot = Spot.objects.create(score=2, name='#1')
        second_best = Spot.objects.create(score=1, name='#2')
        response = self.client.get('/')
        spots = response.context['spots']
        self.assertEqual(spots[0], top_rated_spot)
        self.assertEqual(spots[1], second_best)


class TestSpotDetail(AdvancedTestCase):

    def test_uses_detail_template(self):
        spot = Spot.objects.create()
        response = self.client.get('/spots/details/%d/' % (spot.id,))
        self.assertTemplateUsed(response, 'topspots/detail.html')

    def test_item_title_on_detail_page(self):
        spotname = 'Spotty McSpot'
        spot = Spot.objects.create(name=spotname)
        response = self.client.get('/spots/details/%d/' % (spot.id,))
        self.assertContains(response, spotname)

    def test_user_packs(self):
        user = self.get_logged_in_user()
        spot = Spot.objects.create(name='Test Spot')
        PackLog.objects.create(user=user, spot=spot, packs=3)
        response = self.client.get('/spots/details/%d/' % (spot.id,))
        self.assertEqual(response.context['user_packs'], 3)

    def test_comments_can_be_deleted_by_commentor(self):
        self.get_logged_in_user()
        spot = Spot.objects.create(name='Test Spot')
        comment_text = 'This is an interesting comment'
        self.client.post('/spots/%d/comment/' % (spot.id,), {'comment': comment_text})

        comment = spot.comment_set.first()
        self.assertEqual(comment.text, comment_text)
        self.client.post('/spots/details/%d/comment/%d/delete/' % (spot.id, comment.id))
        self.assertEqual(spot.comment_set.first(), None)


class TestMySpots(AdvancedTestCase):

    def test_uses_myspots_template(self):
        user = self.get_logged_in_user()
        response = self.client.get('/spots/myspots/%s/' % (user.username,))
        self.assertTemplateUsed(response, 'topspots/myspots.html')

    def test_favorite_spot_list(self):
        user = self.get_logged_in_user()
        fav_spot1 = Spot.objects.create(name='Fake Park')
        fav_spot2 = Spot.objects.create(name='Fakest Park')
        user.siteuser.favorites.add(fav_spot1, fav_spot2)
        response = self.client.get('/spots/myspots/%s/' % (user.username,))
        expected_favs = [fav_spot1, fav_spot2]
        actual_favs = [i for i in response.context['favorites']]
        self.assertListEqual(actual_favs, expected_favs)

    def test_favorites_and_packs(self):
        user = self.get_logged_in_user()
        fav_spot = Spot.objects.create(name='Fake Park')
        user.siteuser.favorites.add(fav_spot)
        PackLog.objects.create(user=user, spot=fav_spot, packs=5)
        response = self.client.get('/spots/myspots/%s/' % (user.username,))
        spot = response.context['favorites'][0]
        self.assertEqual(spot, fav_spot)
        self.assertEqual(spot.packs_flown_by(user), 5)


class TestFavorite(AdvancedTestCase):

    def test_favoriting_adds_to_favorites(self):
        user = self.get_logged_in_user()
        self.assertEqual(len(user.siteuser.favorites.all()), 0)
        fav_spot = Spot.objects.create(name='Fake Park')
        self.client.post('/spots/favorite/', {'spot_id': fav_spot.id})
        self.assertEqual(len(user.siteuser.favorites.all()), 1)
        self.assertIn(fav_spot, user.siteuser.favorites.all())

    def test_only_favorite_once(self):
        user = self.get_logged_in_user()
        self.assertEqual(len(user.siteuser.favorites.all()), 0)

        fav_spot = Spot.objects.create(name='Fake Park')
        self.client.post('/spots/favorite/', {'spot_id': fav_spot.id})
        self.assertEqual(len(user.siteuser.favorites.all()), 1)
        self.assertIn(fav_spot, user.siteuser.favorites.all())

    def test_favoriting_increases_score_once_then_decreases(self):
        user = self.get_logged_in_user()
        self.assertEqual(len(user.siteuser.favorites.all()), 0)

        fav_spot = Spot.objects.create(name='Fake Park')
        self.assertEqual(fav_spot.score, 0)
        self.client.post('/spots/favorite/', {'spot_id': fav_spot.id})
        self.assertEqual(Spot.objects.get(id=fav_spot.id).score, 1)

        self.client.post('/spots/favorite/', {'spot_id': fav_spot.id})
        self.assertEqual(Spot.objects.get(id=fav_spot.id).score, 0)


class TestLogPacks(AdvancedTestCase):

    def test_logging_increases_user_total_packs_logged(self):
        user = self.get_logged_in_user()
        self.assertEqual(user.siteuser.total_packs_logged, 0)

        spot = Spot.objects.create(name='Fake Park')
        self.client.post('/spots/%d/logpacks/' % (spot.id,), {'spot_id': spot.id, 'pack-select': 1})

        self.assertEqual(user.siteuser.total_packs_logged, 1)


class TestSaveSpot(AdvancedTestCase):

    def test_spot_saved(self):
        self.get_logged_in_user()
        self.assertEqual(list(Spot.objects.all()), [])

        self.client.post('/spots/save/', {
            'spot_name': 'Fake Park', 'spot_url_name': 'Fake+Park',
            'spot_lat': 38.976605, 'spot_lng': -94.586209
        })
        self.assertEqual(Spot.objects.first().name, 'Fake Park')

    def test_get_redirects_home(self):
        response = self.client.get('/spots/save/')
        self.assertRedirects(response, '/')

    def test_add_to_favs_on_save(self):
        user = self.get_logged_in_user()

        self.client.post('/spots/save/', {
            'spot_name': 'Fake Park', 'spot_url_name': 'Fake+Park',
            'spot_lat': 38.976605, 'spot_lng': -94.586209, 'add_fav': 'on',
        })

        saved_spot = Spot.objects.first()
        self.assertEqual(saved_spot, user.siteuser.favorites.first())

    def test_user_added_spot_count_increases(self):
        user = self.get_logged_in_user()

        self.client.post('/spots/save/', {
            'spot_name': 'Fake Park', 'spot_url_name': 'Fake+Park',
            'spot_lat': 38.976605, 'spot_lng': -94.586209, 'add_fav': 'on',
        })

        saved_spot = Spot.objects.first()
        self.assertEqual(saved_spot.added_by, user)
        self.assertEqual(User.objects.first().spot_set.first(), saved_spot)


class TestNearbySpots(AdvancedTestCase):

    def test_close_spot_not_far_spot(self):
        Spot.objects.create(name='Close Park', latitude=38.976307, longitude=-94.586265)
        Spot.objects.create(name='Closish Park', latitude=38.968842, longitude=-94.579291)
        Spot.objects.create(name='Far Park', latitude=38.937894, longitude=-95.366617)
        response = self.client.get('/spots/search/?search_query=Sunnyside+Park+Summit+Street+KCMO+MO+United +States&miles=5&searchtype=nearby')
        nearby_spots = response.context['nearby_spots']
        self.assertEqual(len(list(nearby_spots)), 2)
        self.assertListEqual(['Close Park', 'Closish Park'], [s.name for s in nearby_spots])

    def test_close_not_close_enough(self):
        Spot.objects.create(name='Close Park', latitude=38.976307, longitude=-94.586265)
        Spot.objects.create(name='Closish Park', latitude=38.968842, longitude=-94.579291)
        Spot.objects.create(name='Not-close-enough Park', latitude=38.969532, longitude=-94.756215)
        response = self.client.get('/spots/search/?search_query=Sunnyside+Park+Summit+Street+KCMO+MO+United +States&miles=5&searchtype=nearby')
        nearby_spots = response.context['nearby_spots']
        self.assertEqual(len(list(nearby_spots)), 2)
        for spot in nearby_spots:
            self.assertLessEqual(spot.distance, 5)
        self.assertListEqual(['Close Park', 'Closish Park'], [s.name for s in nearby_spots])

    def test_exact_coordinates_zero_distance(self):
        lat = 38.977672
        lng = -94.598500
        Spot.objects.create(name='Sunnyside Park', latitude=lat, longitude=lng)
        response = self.client.get('/spots/search/?search_query=Sunnyside+Park+Summit+Street+KCMO+MO+United +States&miles=5&searchtype=nearby'.format(lat, lng))
        nearby_spots = response.context['nearby_spots']
        self.assertEqual(len(list(nearby_spots)), 1)
        self.assertEqual(nearby_spots[0].distance, 0.0)


class TestSpotSearch(AdvancedTestCase):

    def test_spot_search_has_correct_lat_lng(self):
        response = self.client.get('/spots/search/?search_query=Sunnyside+Park+Summit+Street+KCMO+MO+United+States&miles=5&searchtype=default')
        expected_lat = 38.977672
        expected_lng = -94.598500
        self.assertAlmostEqual(response.context['spot_lat'], expected_lat, places=6)
        self.assertAlmostEqual(response.context['spot_lng'], expected_lng, places=6)

    def test_no_search_query_redirects_to_home(self):
        response = self.client.get('/spots/search/?search_query=')
        self.assertRedirects(response, '/')

    def test_existing_spot_search_redirects_to_detail(self):
        lat = 38.977672
        lng = -94.598500
        Spot.objects.create(name='Sunnyside Park', latitude=lat, longitude=lng)
        spot_id = Spot.objects.first().id
        response = self.client.get('/spots/search/?search_query=Sunnyside+Park&searchtype=default')
        self.assertRedirects(response, '/spots/details/%d/' % spot_id)


class TestLogout(AdvancedTestCase):

    def test_logout_logs_out(self):
        self.get_logged_in_user()
        response = self.client.get('/')
        self.assertTrue(response.context['user'].is_authenticated())

        self.client.post('/logout/')
        response = self.client.get('/')
        self.assertFalse(response.context['user'].is_authenticated())

    def test_logout_with_get_doesnt_logout(self):
        self.get_logged_in_user()
        response = self.client.get('/')
        self.assertTrue(response.context['user'].is_authenticated())

        self.client.get('/logout/')
        response = self.client.get('/')
        # User should still be logged in since logout was a GET
        self.assertTrue(response.context['user'].is_authenticated())


class TestSignup(AdvancedTestCase):

    def test_signup_from_detail_redirects_to_detail(self):
        lat = 38.977672
        lng = -94.598500
        Spot.objects.create(name='Sunnyside Park', latitude=lat, longitude=lng)
        spot_id = Spot.objects.first().id

        redirect_to = '/spots/details/%d/' % spot_id

        response = self.client.post('/signup/', {
            'username': 'test_user',
            'password': 'my_password',
            'email': 'email@mail.mail',
            'next': redirect_to,
        })

        self.assertRedirects(response, redirect_to)

    def test_unsafe_next_redirects_home(self):
        redirect_to = 'http://unsafeurl.gov'

        response = self.client.post('/signup/', {
            'username': 'test_user',
            'password': 'my_password',
            'email': 'email@mail.mail',
            'next': redirect_to,
        })

        # If next url is not determined to be safe, redirect to home page
        self.assertRedirects(response, '/')


class TestReplyComment(AdvancedTestCase):

    def test_comment_reply(self):
        self.get_logged_in_user()
        spot = Spot.objects.create(name='Test Spot')
        comment_text = 'This is an interesting comment'
        self.client.post('/spots/%d/comment/' % (spot.id,), {'comment': comment_text})

        comment = Comment.objects.first()
        reply_text = 'this is a reply'
        self.client.post('/spots/details/%d/comment/%d/reply/' % (spot.id, comment.id), {'reply_text': reply_text})

        reply = CommentReply.objects.first()
        self.assertEqual(reply.text, reply_text)

    def test_reply_not_created(self):
        self.get_logged_in_user()
        spot = Spot.objects.create(name='Test Spot')
        comment_text = 'This is an interesting comment'
        self.client.post('/spots/%d/comment/' % (spot.id,), {'comment': comment_text})

        comment = Comment.objects.first()
        self.client.post('/spots/details/%d/comment/%d/reply/' % (spot.id, comment.id), {'reply_text': ''})

        # Reply not created when empty
        self.assertIsNone(CommentReply.objects.first())

        self.client.get('/spots/details/%d/comment/%d/reply/' % (spot.id, comment.id),
                         {'reply_text': 'This is an actual reply'})

        # Reply not created when request is a GET
        self.assertIsNone(CommentReply.objects.first())

        self.client.logout()
        self.client.post('/spots/details/%d/comment/%d/reply/' % (spot.id, comment.id),
                         {'reply_text': 'This is an actual reply'})

        # Reply not created when user not logged in
        self.assertIsNone(CommentReply.objects.first())


class TestDeleteReply(AdvancedTestCase):

    def test_auth_user_delete_reply(self):
        self.get_logged_in_user()
        spot = Spot.objects.create(name='Test Spot')

        comment_text = 'This is an interesting comment'
        self.client.post('/spots/%d/comment/' % (spot.id,), {'comment': comment_text})

        comment = Comment.objects.first()
        reply_text = 'this is a reply'
        self.client.post('/spots/details/%d/comment/%d/reply/' % (spot.id, comment.id), {'reply_text': reply_text})

        reply = CommentReply.objects.first()
        self.assertIsNotNone(reply)

        self.client.post('/spots/details/%d/comment/%d/reply/delete/' % (spot.id, comment.id))
        reply = CommentReply.objects.first()
        self.assertIsNone(reply)

    def test_non_auth_user_delete_reply(self):
        self.get_logged_in_user()
        spot = Spot.objects.create(name='Test Spot')

        comment_text = 'This is an interesting comment'
        self.client.post('/spots/%d/comment/' % (spot.id,), {'comment': comment_text})

        comment = Comment.objects.first()
        reply_text = 'this is a reply'
        self.client.post('/spots/details/%d/comment/%d/reply/' % (spot.id, comment.id), {'reply_text': reply_text})

        self.client.logout()

        self.client.post('/spots/details/%d/comment/%d/reply/delete/' % (spot.id, comment.id))
        reply = CommentReply.objects.first()
        # Reply can't be deleted if user is not logged in
        self.assertIsNotNone(reply)

    def test_get_delete_reply(self):
        self.get_logged_in_user()
        spot = Spot.objects.create(name='Test Spot')

        comment_text = 'This is an interesting comment'
        self.client.post('/spots/%d/comment/' % (spot.id,), {'comment': comment_text})

        comment = Comment.objects.first()
        reply_text = 'this is a reply'
        self.client.post('/spots/details/%d/comment/%d/reply/' % (spot.id, comment.id), {'reply_text': reply_text})

        self.client.get('/spots/details/%d/comment/%d/reply/delete/' % (spot.id, comment.id))
        reply = CommentReply.objects.first()
        # Reply can't be deleted through GET request
        self.assertIsNotNone(reply)

    def test_user_delete_others_reply(self):
        user1 = self.get_logged_in_user(username='user1')
        spot = Spot.objects.create(name='Test Spot')

        comment_text = 'This is an interesting comment'
        self.client.post('/spots/%d/comment/' % (spot.id,), {'comment': comment_text})

        comment = Comment.objects.first()
        reply_text = 'this is a reply'
        self.client.post('/spots/details/%d/comment/%d/reply/' % (spot.id, comment.id), {'reply_text': reply_text})

        self.client.logout()

        user2 = self.get_logged_in_user(username='user2')

        self.client.post('/spots/details/%d/comment/%d/reply/delete/' % (spot.id, comment.id))
        reply = CommentReply.objects.first()
        self.assertEqual(reply.poster, user1)
        self.assertNotEqual(reply.poster, user2)
        # Reply can't be deleted except by user who replied
        self.assertIsNotNone(reply)


class TestEditComment(AdvancedTestCase):

    def test_edit_comment_text(self):
        self.get_logged_in_user()
        spot = Spot.objects.create(name='Test Spot')

        comment_text = 'This is an unedited comment'
        self.client.post('/spots/%d/comment/' % (spot.id,), {'comment': comment_text})

        comment = Comment.objects.first()
        self.assertEqual(comment.text, comment_text)
        edited_text = 'This is an edited comment'
        self.client.post('/spots/details/%d/comment/%d/edit/' % (spot.id, comment.id),
                         {'text': edited_text, 'edit_type': 'comment'})
        self.assertEqual(Comment.objects.get(id=comment.id).text, edited_text)

    def test_edit_reply_text(self):
        self.get_logged_in_user()
        spot = Spot.objects.create(name='Test Spot')

        comment_text = 'This is an unedited comment'
        self.client.post('/spots/%d/comment/' % (spot.id,), {'comment': comment_text})

        comment = Comment.objects.first()

        self.client.post('/spots/details/%d/comment/%d/reply/' % (spot.id, comment.id),
                         {'reply_text': 'This is an unedited reply'})

        reply = CommentReply.objects.first()
        self.assertEqual(comment.commentreply_set.first(), reply)

        edited_text = 'This is an edited reply'
        self.client.post('/spots/details/%d/comment/%d/edit/' % (spot.id, reply.id),
                         {'text': edited_text, 'edit_type': 'reply'})

        self.assertEqual(CommentReply.objects.first().text, edited_text)


class TestUpdateLocation(AdvancedTestCase):

    def test_user_starts_with_no_location(self):
        user = self.get_logged_in_user()
        self.assertEqual((user.siteuser.latitude, user.siteuser.longitude), (None, None))

    def test_update_user_location(self):
        user = self.get_logged_in_user()
        self.assertEqual(user.siteuser.address, None)
        address = '64114'
        self.client.post('/updatelocation/', {'address': address})
        s_user = User.objects.get(id=user.id).siteuser
        self.assertEqual(s_user.address, address)
        self.assertAlmostEqual(float(s_user.latitude), 38.9546734, places=6)
        self.assertAlmostEqual(float(s_user.longitude), -94.5985613, places=6)

    def test_use_address_for_location(self):
        user = self.get_logged_in_user()
        self.assertFalse(user.siteuser.use_address)
        self.client.post('/useaddress/', {'use_address': 'on'})
        self.assertTrue(User.objects.get(id=user.id).siteuser.use_address)
        self.client.post('/useaddress/', {'use_address': ''})
        self.assertFalse(User.objects.get(id=user.id).siteuser.use_address)
