from datetime import datetime

from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib import auth
from django.utils.http import is_safe_url
from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect

from topspots.models import Spot, PackLog, Comment, SiteUser,\
    get_map_url, CommentReply, GMAPS_JS_API_KEY

from topspots.forms import SignUpForm
from topspots.services import get_weather_conditions, get_nearby_airports,\
    get_lat_lng, get_city_state, get_location_from_address
from topspots.helpers import get_nearby_spots, paginate_query, geolocate,\
    urlize_spot_name


def authenticate_and_login(request, username, password):
    user = auth.authenticate(username=username, password=password)
    auth.login(request, user)


def home_page(request):
    '''Display top rated spots on home page'''
    sorting = request.GET.get('sort')
    all_spots = Spot.objects.all()
    user = request.user.siteuser if request.user.is_authenticated() else None

    if user and all((user.use_address, user.latitude, user.longitude)):
        user_lat = lat = user.latitude
        user_lng = lng = user.longitude
        geolocate_fail = False
    else:
        lat, lng, geolocate_fail = geolocate(request)

        user_lat = lat if not geolocate_fail else None
        user_lng = lng if not geolocate_fail else None

    if not sorting or sorting == 'near':

        spot_list = list(get_nearby_spots(lat, lng, within_miles=100))

        # Don't show nearby spots as default on home page if there are none
        sorting = 'near' if spot_list or sorting else 'favorites'

    if sorting == 'recent':
        spot_list = Spot.objects.order_by('-created', '-score')
    elif sorting == 'favorites':
        spot_list = Spot.objects.order_by('-score', '-created')

    page = request.GET.get('page')
    spots = paginate_query(page, spot_list, 10)
    spot_count = Spot.objects.count()
    user_count = SiteUser.objects.count()
    return render(request, 'topspots/home.html', {
        'spots': spots,
        'spot_count': spot_count,
        'sorting': sorting,
        'user_count': user_count,
        'geolocate_fail': geolocate_fail,
        'all_spots': all_spots,
        'user_lat': user_lat,
        'user_lng': user_lng,
    })


def spot_detail(request, spot_id):
    '''Display details of spot'''
    spot = Spot.objects.get(id=spot_id)
    pack_numbers = range(1, 101)
    user = request.user
    user_packs = spot.packs_flown_by(user) if user.is_authenticated() else 0
    conditions = get_weather_conditions(lat=spot.latitude, lng=spot.longitude)
    airports = get_nearby_airports(lat=spot.latitude, lng=spot.longitude)
    weather_checked_at = datetime.fromtimestamp(conditions['time'])
    return render(request, 'topspots/detail.html', {
        'spot': spot,
        'packs': pack_numbers,
        'user_packs': user_packs,
        'rain': conditions['rain'],
        'wind': conditions['wind'],
        'temp': conditions['temp'],
        'weather_checked_at': weather_checked_at,
        'airports': airports,
    })


def spot_search(request):
    spot_fullname = request.GET['search_query']
    # User has not entered a search query
    if not spot_fullname:
        return HttpResponseRedirect('/')

    latlng = request.GET.get('latlng')
    if latlng:
        spot_lat, spot_lng = [float(i) for i in latlng.split(',')]
    else:
        spot_lat, spot_lng = get_lat_lng(spot_fullname)

    if request.GET.get('searchtype') == 'nearby':
        # todo update so that user can enter miles
        miles = request.GET.get('miles') or 10

        # Find spots with miles of spot_lat and spot_lng
        nearby_spots_list = get_nearby_spots(spot_lat, spot_lng, miles)
        page = request.GET.get('page')
        nearby_spots = paginate_query(page, list(nearby_spots_list), 10)
        return render(request, 'topspots/nearbyspots.html', {
            'nearby_spots': nearby_spots,
            'nearby_spot_count': len(list(nearby_spots)),
            'miles_radius': miles,
            'ref_point': spot_fullname,
        })

    try:
        spot = Spot.objects.get(name=spot_fullname)
        return HttpResponseRedirect(reverse('spot_detail', args=(spot.id,)))
    except Spot.DoesNotExist:
        airports = get_nearby_airports(spot_lat, spot_lng)
        spot_url_name = urlize_spot_name(spot_fullname)
        static_url = get_map_url(
            spot_url_name, latlng=latlng, api_key=GMAPS_JS_API_KEY
        )
        place_url = 'https://www.google.com/maps/place/' + spot_url_name
        return render(request, 'topspots/spotsearch.html', {
            'spot_name': spot_fullname,
            'spot_url_name': spot_url_name,
            'static_url': static_url,
            'spot_lat': spot_lat,
            'spot_lng': spot_lng,
            'airports': airports,
            'place_url': place_url,
        })


def save_spot(request):
    if request.method == 'POST':
        spot_name = request.POST['spot_name']
        url_name = request.POST['spot_url_name']
        latitude = request.POST['spot_lat']
        longitude = request.POST['spot_lng']
        city, state = get_city_state(latitude, longitude)
        spot = Spot(
            name=spot_name,
            longitude=longitude,
            latitude=latitude,
            url_name=url_name,
            city=city,
            state=state,
            added_by=request.user,
        )
        spot.save()
        user = request.user

        if request.POST.get('add_fav') == 'on' and user.is_authenticated():
            user.siteuser.favorites.add(spot)
            spot.score += 1
            spot.save()
        return HttpResponseRedirect(reverse('spot_detail', args=(spot.id,)))
    return HttpResponseRedirect('/')


@login_required
def favorite(request):
    user = request.user
    spot_id = request.POST['spot_id']
    spot = Spot.objects.get(id=spot_id)
    if not spot.favorited_by(user.siteuser):
        spot.siteuser_set.add(user.siteuser)
        spot.score += 1
        spot.save()
    else:
        spot.siteuser_set.remove(user.siteuser)
        spot.score -= 1
        spot.save()
    return JsonResponse({'status': 'ok'})


def logout(request):
    if request.user.is_authenticated() and request.method == 'POST':
        auth.logout(request)
    return HttpResponseRedirect(reverse('home'))


def signup(request):
    icons = {
        'Username': 'user', 'Email address': 'envelope', 'Password': 'lock'
    }

    if request.method == 'POST':

        form = SignUpForm(request.POST)

        if form.is_valid():

            redirect_to = request.POST.get('next', '/')
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = '/'

            password = form.cleaned_data['password']
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']

            User.objects.create_user(
                username=username, email=email, password=password
            )
            authenticate_and_login(request, username, password)
            messages.success(request, 'User signup successful.')
            return HttpResponseRedirect(redirect_to)
    else:
        form = SignUpForm()

    return render(request, 'topspots/signup.html', {
        'form': form, 'icons': icons, 'next': request.GET.get('next', '/')
    })


@login_required
def comment(request, spot_id):
    if request.POST.get('comment'):
        spot = Spot.objects.get(id=spot_id)
        Comment.objects.create(
            text=request.POST['comment'],
            poster=request.user,
            spot=spot,
        )
    return HttpResponseRedirect(reverse('spot_detail', args=(spot_id,)))


@login_required
def my_spots(request, username):
    user = User.objects.get(username=username)
    favorites = user.siteuser.favorites.order_by('-score')
    return render(request, 'topspots/myspots.html', {'favorites': favorites})


@login_required
def log_packs(request, spot_id):
    packs = request.POST['pack-select']
    spot = Spot.objects.get(id=spot_id)
    PackLog.objects.create(user=request.user, spot=spot, packs=packs)
    return HttpResponseRedirect(reverse('spot_detail', args=(spot_id,)))


@login_required
def delete_comment(request, spot_id, comment_id):
    if request.user.is_authenticated() and request.method == 'POST':
        Comment.objects.filter(id=comment_id).delete()
    return HttpResponseRedirect(reverse('spot_detail', args=(spot_id,)))


@login_required
def reply_comment(request, spot_id, comment_id):
    if request.user.is_authenticated() and request.method == 'POST':
        reply_text = request.POST.get('reply_text')
        if reply_text:
            comment = Comment.objects.get(id=comment_id)
            CommentReply.objects.create(
                text=reply_text, comment=comment, poster=request.user
            )

    return HttpResponseRedirect(reverse('spot_detail', args=(spot_id,)))


@login_required
def delete_reply(request, spot_id, reply_id):
    user = request.user
    if user.is_authenticated() and request.method == 'POST':
        reply = CommentReply.objects.get(id=reply_id)
        if user.id == reply.poster.id:
            reply.delete()
    return HttpResponseRedirect(reverse('spot_detail', args=(spot_id,)))


@login_required
def edit_comment(request, spot_id, comment_id):
    if request.user.is_authenticated() and request.method == 'POST':
        edit_type = request.POST['edit_type']
        _Model = Comment if edit_type == 'comment' else CommentReply
        comment_or_reply = _Model.objects.get(id=comment_id)
        comment_or_reply.text = request.POST['text']
        comment_or_reply.save()

    return HttpResponseRedirect(reverse('spot_detail', args=(spot_id,)))


@login_required
def user_profile(request, user_id):
    if int(user_id) != request.user.id:
        return HttpResponseRedirect('/')
    return render(request, 'topspots/profile.html')


def go_home(request):
    return HttpResponseRedirect('/')


def help_page(request):
    return render(request, 'topspots/help.html')


def tips(request):
    return render(request, 'topspots/tipsandtricks.html')


def submit_video_link(request, spot_id):
    link = request.POST.get('video_link')
    if link:
        spot = Spot.objects.get(id=spot_id)
        email_message = '''
        Video to be featured on page for  %s:\n
        %s
        ''' % (spot.name, link)
        send_mail(
            'user featured video submission',
            email_message,
            'miniquadmaps@gmail.com',
            ['miniquadmaps@gmail.com'],
            fail_silently=False,
        )
    return HttpResponseRedirect(reverse('spot_detail', args=(spot_id,)))


@login_required
def update_location(request):
    if request.method == 'POST':
        address = request.POST.get('address')
        s_user = request.user.siteuser
        s_user.address = address
        lat, lng = get_location_from_address(address)
        s_user.latitude = lat
        s_user.longitude = lng
        s_user.save()

    return HttpResponseRedirect(
        reverse('user_profile', args=(request.user.id,))
    )


def use_address(request):
    if request.method == 'POST':
        s_user = request.user.siteuser
        use = request.POST.get('use_address')
        s_user.use_address = True if use == 'on' else False
        s_user.save()
    return HttpResponseRedirect(
        reverse('user_profile', args=(request.user.id,))
    )
