import requests
import os

from topspots.helpers import urlize_spot_name

GMAPS_JS_API_KEY = os.environ['GMAPS_JS_API_KEY']
GMAPS_WEB_GEO_API_KEY = os.environ['GMAPS_WEB_GEO_API_KEY']

def get_weather_conditions(lat, lng):
    '''Get current weather conditions from forecast api'''
    url = 'https://api.forecast.io/forecast/{api_key}/{lat},{lng}'.format(
        api_key=os.environ['FORECAST_API_KEY'],
        lat=lat,
        lng=lng,
    )
    r = requests.get(url)
    current_forecast = r.json()['currently']
    wind_speed = current_forecast['windSpeed']
    chance_rain = int(current_forecast['precipProbability'] * 100)
    temp = current_forecast['temperature']
    time = current_forecast['time']
    return {'wind': wind_speed, 'rain': chance_rain, 'time': time, 'temp': temp}


def get_lat_lng(location_name):
    '''Return lat, lng tuple from address'''
    url = 'https://maps.googleapis.com/maps/api/geocode/json?address={address}&key={api_key}'
    r = requests.get(url.format(address=location_name, api_key=GMAPS_WEB_GEO_API_KEY))
    location_data = r.json()
    if location_data['status'] == 'ZERO_RESULTS':
        return (None, None)
    lat_lng = location_data['results'][0]['geometry']['location']
    return (lat_lng['lat'], lat_lng['lng'])


def get_city_state(lat, lng):
    url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng={lat},{lng}&key={api_key}'
    r = requests.get(url.format(lat=lat, lng=lng, api_key=GMAPS_WEB_GEO_API_KEY))
    results = r.json()['results']
    location_data = results[0]['address_components'] if results else []
    city = state = ''
    for comp in location_data:
        if 'administrative_area_level_1' in comp['types']:
            state = comp['short_name']
        elif 'locality' in comp['types']:
            city = comp['long_name']
    return (city, state)


def get_nearby_airports(lat, lng):
    url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={lat},{lng}&key={api_key}&radius=8046.72&type=airport'
    r = requests.get(url.format(lat=lat, lng=lng, api_key=GMAPS_WEB_GEO_API_KEY))
    airport_data = r.json()['results']
    airport_list = []
    for ap in airport_data:
        if ap['name'].lower().endswith('airport'):
            airport_list.append(ap['name'])
    return airport_list


def geolocate_with_google():
    url = "https://www.googleapis.com/geolocation/v1/geolocate?key=%s" % GMAPS_JS_API_KEY
    r = requests.post(url)
    if 'error' in r.json():
        # Set location to downtown Kansas city if geolocation fails
        return (39.096915, -94.572250, True)
    lat_lng = r.json()['location']
    return (lat_lng['lat'], lat_lng['lng'], False)


def get_location_from_address(address):
    '''Return lat, lng tuple from an address'''
    address = urlize_spot_name(address)
    url = 'https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}'
    r = requests.get(url.format(address, GMAPS_WEB_GEO_API_KEY))

    if r.json()['status'] != 'OK':
        return (None, None)
    lat_lng = r.json()['results'][0]['geometry']['location']
    return (lat_lng['lat'], lat_lng['lng'])
