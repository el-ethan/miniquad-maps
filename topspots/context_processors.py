import os

import config

from topspots.models import GMAPS_JS_API_KEY


def api_keys(request):
    return {
        'gmaps_js_api_key': GMAPS_JS_API_KEY,
    }
