import re

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.gis.geoip2 import GeoIP2, GeoIP2Exception

from ipware.ip import get_real_ip

from topspots.models import Spot


def get_nearby_spots(lat, lng, within_miles):
    return Spot.objects.raw(
        'SELECT id, ( 3959 * acos( cos( radians(%(ref_lat)s) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(%(ref_lng)s) ) + sin( radians(%(ref_lat)s) ) * sin( radians( latitude ) ) ) ) AS distance FROM topspots_spot HAVING distance < %(miles)s ORDER BY distance;',
        {'ref_lat': lat, 'ref_lng': lng, 'miles': within_miles},
        )


def paginate_query(page, query, per_page):
    paginator = Paginator(query, per_page)
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        items = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        items = paginator.page(paginator.num_pages)

    return items


def geolocate(request):
    ip = get_real_ip(request)
    if ip is not None:
        g = GeoIP2()

    try:
        lat, lng = g.lat_lon(ip)
        return (lat, lng, False)
    except:
        pass

    # Set location to downtown Kansas city if geolocation fails
    return (39.096915, -94.572250, True)


def urlize_spot_name(spot_name):
    return re.sub(r'([ ,]+)', '+', spot_name)
