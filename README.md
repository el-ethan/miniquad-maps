# Miniquad Maps [![Build Status](https://travis-ci.org/el-ethan/miniquad-maps.svg?branch=develop)](https://travis-ci.org/el-ethan/miniquad-maps)

A Django site for sharing and discovering places to fly your miniquad.
